# -*- coding: utf-8 -*-
"""
Created on Tue Jul 31 10:45:25 2018

@author: Hamid
"""
from scipy.special import logit
import numpy as np 
def initialize_parameters_random(num_X,num_hid):             
    parameters = {}
    parameters['W'] = np.random.rand(num_X,num_hid)
    parameters['b'] = np.random.rand(1,num_hid)
    return parameters 
#%%
def sigmoid(Z):
    A = 1/(1+np.exp(-Z))
    return A
#%%
def softmax(Z):
    A = np.exp(Z) / np.sum(np.exp(Z), axis=0,keepdims = True)
    return A 
#%%
def DeepELM_wights(X_train , Y_train , num_hid , num_layer , C): 
    param = initialize_parameters_random(X_train.shape[1],num_hid)
    ones = np.ones((X_train.shape[0],1))
    X_train_n = np.column_stack([X_train,ones])
    W = param [ 'W' ]
    b = param [ 'b']
    W_E = np.row_stack([W,b])
    if W_E.shape[0] >W_E.shape[1]:
          v,s,u =np.linalg.svd(W_E)
          weights=v[:,0:W_E.shape[1]]
    else :
          W_E = W_E.T
          v,s,u =np.linalg.svd(W_E)
          temp = v[:,0:W_E.shape[1]]
          weights = temp.T 
    weights_h = np.zeros((num_layer,num_hid+1,num_hid))
    H,Beta = ELM_Train(X_train_n,Y_train,num_hid,C,weights)
    ones_temp = np.ones((H.shape[0],1))
    HE=np.column_stack([H,ones_temp])
    beta_inverse = sudo_inverse(Beta,C)
    H1=np.dot(Y_train,beta_inverse)
    max_h1 = np.max(H1)
    min_h1 = np.min(H1)
    H1_normal = normalize(H1,0.1,0.9)
    HE_inverse = sudo_inverse(HE,C)
    inverse_activation = logit(H1_normal)
    H1_normal_back = normalize(inverse_activation,min_h1,max_h1)
    weights_h[0,:,:]=np.dot(HE_inverse,H1_normal_back)
    for i in range(num_layer-1):
        H,Beta =ELM_Train(HE,Y_train,num_hid,C,weights_h[0,:,:]) 
        ones_temp = np.ones((H.shape[0],1))
        HE=np.column_stack([H,ones_temp])
        beta_inverse = sudo_inverse(Beta,C)
        H1=np.dot(Y_train,beta_inverse)
        max_h1 = np.max(H1)
        min_h1 = np.min(H1)
        H1_normal = normalize(H1,0.1,0.9)
        HE_inverse = sudo_inverse(HE,C)
        inverse_activation = logit(H1_normal)
        H1_normal_back = normalize(inverse_activation,min_h1,max_h1)
        weights_h[i+1,:,:]=np.dot(HE_inverse,H1_normal_back)
    return weights_h,weights
#%%   
def Deep_ELM_Train(X_train,Y_train , weight_h,weights,n_hid,C):
    h1 = forward_pro(X_train,weights[0:X_train.shape[1],:],weights[X_train.shape[1],:])
    for i in range(weight_h.shape[0]):  
        h2 = forward_pro(h1,weight_h[i,0:n_hid,:],weight_h[i,n_hid,:])
        h1=h2
    H_n=np.dot(h2.T,h2)
    one_matrix=np.identity(H_n.shape[0])
    one_matrix=one_matrix* 1/C
    new_H=H_n + one_matrix
    #moore_inv=np.linalg.pinv(H)
    inverse_H=np.linalg.inv(new_H)
    Beta_hat=np.dot(np.dot(inverse_H,h2.T),Y_train)    
    return Beta_hat     
#%%
def Deep_ELM_Test(X_test,weight_h,weights,Beta,n_hid):
    h1 = forward_pro(X_test,weights[0:X_test.shape[1],:],weights[X_test.shape[1],:])
    for i in range(weight_h.shape[0]):  
        h2 = forward_pro(h1,weight_h[i,0:n_hid,:],weight_h[i,n_hid,:])
        h1=h2
    Y_predict=np.dot(h2,Beta)  
    Y_predict=softmax(Y_predict)
    return Y_predict         
#%%
def sudo_inverse (X,C):
    X_n = np.dot(X.T,X)
    one_matrix=np.identity(X_n.shape[0])
    one_matrix=one_matrix* 1/C
    new_X=X_n + one_matrix
    inverse_X=np.linalg.inv(new_X)
    invX = np.dot(inverse_X,X.T)
    return invX
#%%
def normalize(X,a,b):
    t1 = b-a
    Xmin= np.min(X)
    Xmax = np .max(X)
    t2 = X-Xmin
    t3 = Xmax - Xmin
    t4 = t2 / t3
    t5 = t1 * t4
    return t5+a         
#%%
def ELM_Train(X_train,Y_train,num_hid,C,W):
    temp_H=(np.dot(X_train,W))
    temp_H=temp_H/np.max(temp_H)
    H=sigmoid(temp_H)
    H_n=np.dot(H.T,H)
    one_matrix=np.identity(H_n.shape[0])
    one_matrix=one_matrix* 1/C
    new_H=H_n + one_matrix
    #moore_inv=np.linalg.pinv(H)
    inverse_H=np.linalg.inv(new_H)
    Beta_hat=np.dot(np.dot(inverse_H,H.T),Y_train)
    return H,Beta_hat
#%%
def forward_pro(X,W,b):
    temp_H=(np.dot(X,W)+b)
    temp_H=temp_H/np.max(temp_H)
    h=sigmoid(temp_H) 
    return h
#%%       