# -*- coding: utf-8 -*-
"""
Created on Sat Aug 18 12:46:17 2018

@author: Hamid
"""
from basic_Autoencoder_with_inv_function import multi_network_train,multi_network_test
from ConvertToonehot import convert_to_one_hot
from  dataset import load_mnist
from basic_elm_deep import fusion_ELM_deep,predict_new
def elm_deep_main():
    mnist=load_mnist()
    X_train_mnist = mnist.train.images
    train_x = X_train_mnist.T
    y_train_mnist = mnist.train.labels
    X_test_mnist =  mnist.test.images
    test_x = X_test_mnist.T
    y_test_mnist = mnist.test.labels   
    train_y = convert_to_one_hot(y_train_mnist, 10)
    test_y = convert_to_one_hot(y_test_mnist, 10)
    g="sigmoid"
    num_layer=5
    max_loop=5
    D=5
    C=10**6
    HC_train,reconst_x,af_list,bf_list =multi_network_train(train_x.T , g,num_layer , max_loop , D ,C)
    parameters,g,af_list,bf_list = fusion_ELM_deep(train_x, train_y,D,"sigmoid",af_list,bf_list,learning_rate =  0.001, num_iterations = 1, print_cost = True,beta1=0.99,beta2 = 0.999,epsilon = 1e-8,mini_batch_size = 64)
    pred_train = predict_new(train_x, train_y, parameters,"sigmoid",af_list,bf_list )
    pred_test = predict_new(test_x, test_y, parameters,"sigmoid",af_list,bf_list )
    return pred_train,pred_test
#%%
pred_train,pred_test=elm_deep_main()    