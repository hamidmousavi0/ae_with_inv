# -*- coding: utf-8 -*-
"""
Created on Fri Jul 13 11:19:42 2018

@author: Hamid
"""
import numpy as np 
from basic_elm import ELM_train,ELM_test,initialize_parameters_random,sigmoid,softmax
def DrElm_train(X_train,Y_train,hid_num,k,alpha,C):
    W1i=np.zeros((k,X_train.shape[1],hid_num))
    b1i=np.zeros((k,1,hid_num))
    Bi=np.zeros((k,hid_num,Y_train.shape[1]))
    Oi=np.zeros((k,X_train.shape[0],Y_train.shape[1]))
    W2i=np.zeros((k,Y_train.shape[1],X_train.shape[1]))
    Xinew=np.zeros((k+1,X_train.shape[0],X_train.shape[1]))
    Xinew[0,:,:]=X_train   
    for i in range(k):
        W1i[i,:,:],Bi[i,:,:],b1i[i,:,:],Oi[i,:,:]=ELM_train(Xinew[i,:,:],Y_train,hid_num,C[i])
        parameter=initialize_parameters_random(Y_train.shape[1],X_train.shape[1])
        W2i[i,:,:]=parameter['W']
        Xinew[i+1,:,:]=(Xinew[0,:,:]+(alpha*np.dot(Oi[i,:,:],W2i[i,:,:])))
        Xinew=Xinew/np.max(Xinew)
    temp_H=(np.dot(Xinew[k,:,:],W1i[k-1,:,:])+b1i[k-1,:,:]) 
    temp_H=temp_H/np.max(temp_H)
    H=sigmoid(temp_H)
    Y=np.dot(H,Bi[k-1,:,:])  
    Y=softmax(Y)
    return Xinew,W1i ,Bi ,W2i,b1i,Y 
#%%
def DrElm_test(X,Y_train,hid_num,k,alpha,W1i,Bi,W2i,b1i):
    Xinew=np.zeros((k+1,X.shape[0],X.shape[1]))
    Oi=np.zeros((k,X.shape[0],Y_train.shape[1]))
    Xinew[0,:,:]=X
    for i in range(k):
         Oi[i,:,:]=ELM_test(Xinew[i,:,:],W1i[i,:,:],b1i[i,:,:],Bi[i,:,:])
         Xinew[i+1,:,:]=(Xinew[0,:,:]+(alpha*np.dot(Oi[i,:,:],W2i[i,:,:])))
         Xinew=Xinew/np.max(Xinew)
    temp_H=(np.dot(Xinew[k,:,:],W1i[k-1,:,:])+b1i[k-1,:,:]) 
    temp_H=temp_H/np.max(temp_H)
    H=sigmoid(temp_H)
    Y_predict=np.dot(H,Bi[k-1,:,:])
    Y_predict=softmax(Y_predict)
    return Y_predict            