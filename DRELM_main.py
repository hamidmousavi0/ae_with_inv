# -*- coding: utf-8 -*-
"""
Created on Sat Jul 21 14:58:50 2018

@author: admin
"""
from dataset import load_mnist
import numpy as np
from sklearn.model_selection import KFold 
from predict import predict_new,convert_to_one_hot
from basic_DRELM import DrElm_test,DrElm_train
def DRELM_main():  
    mnist=load_mnist()
    X_train_mnist = mnist.train.images
    y_train_mnist = mnist.train.labels
    X_test_mnist =  mnist.test.images
    y_test_mnist = mnist.test.labels   
    #L_M=[10*i for i in range(2,201)]

    L_M=[2000]
    C1=[10**6,10**6,10**5,10**5,10**4,10**4,10**3,10**3,10**2,10**2]
    C=[C1]
    #L_O=[10*i for i in range(2,21)]
    acc=np.zeros((5))
    accuracy=np.zeros((50))
    pred_chain=np.zeros((len(C)))
    for k in range(len(L_M)):
        print(k)
        for kk in range(len(C)):
            print(kk)
            kf = KFold(n_splits=5)
            kf_i=0
            for train_index, test_index in kf.split(X_train_mnist):
                Train_X, Test_X = X_train_mnist[train_index], X_train_mnist[test_index]
                Train_Y, Test_Y = y_train_mnist[train_index], y_train_mnist[test_index]
                Train_Y=convert_to_one_hot(Train_Y,10).T
                Test_Y=convert_to_one_hot(Test_Y,10).T
                Xinew,W1i ,Bi ,W2i,b1i,Y=DrElm_train(Train_X,Train_Y,L_M[k],10,0.1,C[kk])
                Y_predict=DrElm_test(Test_X,Test_Y,L_M[k],10,0.1,W1i,Bi,W2i,b1i)
                acc[kf_i]=predict_new(Test_Y,Y_predict)
                kf_i=kf_i+1
            pred_chain[k]=np.sum(acc)/5  
    n_hid=2000#L_M[np.argmax(pred_chain)]   
    CC=[10**6,10**6,10**5,10**5,10**4,10**4,10**3,10**3,10**2,10**2]
    y_train_mnist=convert_to_one_hot(y_train_mnist,10).T
    y_test_mnist=convert_to_one_hot(y_test_mnist,10).T 
    X_train_mnist=X_train_mnist
    X_test_mnist=X_test_mnist
    for i in range(50):
         Xinew,W1i ,Bi ,W2i,b1i,Y=DrElm_train(X_train_mnist,y_train_mnist,n_hid,10,0.1,CC)
         Y_predict=DrElm_test(X_test_mnist,y_test_mnist,n_hid,10,0.1,W1i,Bi,W2i,b1i)
         accuracy[i]=predict_new(y_test_mnist,Y_predict)
    final_acc= np.sum(accuracy)/50   
    return final_acc