# -*- coding: utf-8 -*-
"""
Created on Sat Jul 21 14:26:38 2018

@author: admin
"""
import numpy as np
def ELM_train(X_train,Y_train,num_hid,C):
    
#    c=100000
    parameter=initialize_parameters_random(X_train.shape[1],num_hid)
    W=parameter['W']
    b=parameter['b']
    temp_H=(np.dot(X_train,W)+b)
    temp_H=temp_H/np.max(temp_H)
    H=sigmoid(temp_H)
    H_n=np.dot(H.T,H)
    one_matrix=np.identity(H_n.shape[0])
    one_matrix=one_matrix* 1/C
    new_H=H_n + one_matrix
    #moore_inv=np.linalg.pinv(H)
    inverse_H=np.linalg.inv(new_H)
    Beta_hat=np.dot(np.dot(inverse_H,H.T),Y_train)
    Y=np.dot(H,Beta_hat)
    Y=softmax(Y)
    return W,Beta_hat,b,Y
#%%
def ELM_test(X,W,b,Beta_hat):
    temp_H=(np.dot(X,W)+b)
    temp_H=temp_H/np.max(temp_H)
    h=sigmoid(temp_H)
    Y_predict=np.dot(h,Beta_hat)
    Y_predict=softmax(Y_predict)
    return Y_predict  
#%%
def sigmoid(Z):
    A = 1/(1+np.exp(-Z))
    return A
#%%
def softmax(Z):
    A = np.exp(Z) / np.sum(np.exp(Z), axis=1,keepdims = True)
    return A 
#%%
def gaussian(Z):
     A=np.exp(-pow(Z,2.0))
     return A
#%%
def relu(Z):
    A = np.maximum(0,Z)
    return A    
#%%
def initialize_parameters_random(num_X,num_hid):             
    parameters = {}
#    parameters['W'] = np.random.uniform(np.sqrt(-6/num_hid+num_X),np.sqrt(6/num_hid+num_X),(num_X,num_hid))
    parameters['W'] = np.random.rand(num_X,num_hid)
    parameters['b'] = np.random.rand(1,num_hid)
    return parameters   
#%%    