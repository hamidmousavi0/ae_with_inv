# -*- coding: utf-8 -*-
"""
Created on Tue Jul 31 10:45:20 2018

@author: Hamid
"""
from basic_TELM import Deep_ELM_Train,Deep_ELM_Test,DeepELM_wights
from sklearn.model_selection import KFold 
from predict import predict_new,convert_to_one_hot
import numpy as np 
from  dataset import load_mnist,complex_mmodal_nonlinear2
from dataset import load_optdigits,load_segment,load_vowel
from dataset import load_pendigits,load_sataleit
from dataset import sing_unimodal_nonli,complex_mmodal_nonlinear
from sklearn.metrics import mean_squared_error
from math import sqrt
from sklearn.model_selection import train_test_split
def ELM_main():    
#    CX_train,CY_train,CX_test,CY_test=complex_mmodal_nonlinear(1000,10)
#    C2X_train,C2Y_train,C2X_test,C2Y_test=complex_mmodal_nonlinear2(1000,10)
#    SX_train,SY_train,SX_test,SY_test=sing_unimodal_nonli(1000,10)
#    vowel_data = load_vowel()
#    VX_train = vowel_data[:,0:13]
#    VX_train = VX_train/np.max(VX_train)
#    VY_train = vowel_data[:,13]
#    VX_train, VX_test, Vy_train, Vy_test = train_test_split(VX_train, VY_train, test_size=0.33, random_state=42)
#    Vy_train=Vy_train.reshape((663,1))
#    Vy_train=Vy_train.astype(np.int64)
#    Vy_test=Vy_test.reshape((327,1))
#    Vy_test=Vy_test.astype(np.int64)
#    staleit_train ,staleit_test  =load_sataleit()
#    SAX_train=staleit_train[:,0:36]
#    SAX_train = (SAX_train-np.min(SAX_train))/(np.max(SAX_train)-np.min(SAX_train))
#    SAY_train=staleit_train[:,36]
#    SAX_test=staleit_test[:,0:36]
#    SAX_test = (SAX_test-np.min(SAX_test))/(np.max(SAX_test)-np.min(SAX_test))
#    SAY_test=staleit_test[:,36]
#    SAY_train=SAY_train.reshape((4435,1))
#    SAY_train=SAY_train.astype(np.int64)
#    SAY_test=SAY_test.reshape((2000,1))
#    SAY_test=SAY_test.astype(np.int64)
    pendigits_train ,pendigits_test  =load_pendigits()
    PX_train=pendigits_train[:,0:16]
    PX_train = (PX_train-np.min(PX_train))/(np.max(PX_train)-np.min(PX_train))
    PY_train=pendigits_train[:,16]
    PX_test=pendigits_test[:,0:16]
    PX_test = (PX_test-np.min(PX_test))/(np.max(PX_test)-np.min(PX_test))
    PY_test=pendigits_test[:,16]
    PY_train=PY_train.reshape((7494,1))
    PY_train=PY_train.astype(np.int64)
    PY_test=PY_test.reshape((3498,1))
    PY_test=PY_test.astype(np.int64)
#    optdigits_train ,optdigits_test = load_optdigits()
#    OX_train=optdigits_train[:,0:64]
#    OX_train = (OX_train-np.min(OX_train))/(np.max(OX_train)-np.min(OX_train))
#    OY_train=optdigits_train[:,64]
#    OX_test=pendigits_test[:,0:64]
#    OX_test = (OX_test-np.min(OX_test))/(np.max(OX_test)-np.min(OX_test))
#    OY_test=optdigits_test[:,64]
#    segment=load_segment()
#    SEX_train = segment[:,0:19]
#    SEX_train = (SEX_train-np.min(SEX_train))/(np.max(SEX_train)-np.min(SEX_train))
#    SEY_train = segment[:,19]
#    SEX_train, SEX_test, SEy_train, SEy_test = train_test_split(SEX_train, SEY_train, test_size=0.33, random_state=42)
#    mnist = load_mnist()
#    X_train_mnist = mnist.train.images
#    y_train_mnist = mnist.train.labels
#    X_test_mnist =  mnist.test.images
#    y_test_mnist = mnist.test.labels   
    L_M=[100]
    num_layer=[10]
    C=[10**6]
    acc=np.zeros((5))
#    RMS=np.zeros((5))
#    pred_RMSE=np.zeros(len(num_layer))
    accuracy_test=np.zeros((20))
    accuracy_train=np.zeros((20))
#    RMSE_test=np.zeros((20))
#    RMSE_train=np.zeros((20))
    pred_chain=np.zeros(len(num_layer))
    for k in range(len(num_layer)):
        print("baraye",num_layer[k])
        for kk in range(len(C)):
            print("baraye",C[kk])
            kf = KFold(n_splits=5)
            kf_i=0
            for train_index, test_index in kf.split(PX_train):
                Train_X, Test_X = PX_train[train_index], PX_train[test_index]
                Train_Y, Test_Y = PY_train[train_index], PY_train[test_index]
                Train_Y=convert_to_one_hot(Train_Y,10).T
                Test_Y=convert_to_one_hot(Test_Y,10).T
                weigt,w = DeepELM_wights(Train_X , Train_Y , L_M[0] , num_layer[k] , C[kk]) 
                beta_hat = Deep_ELM_Train(Train_X,Train_Y , weigt,w,L_M[0],C[kk])
                Y_predict=Deep_ELM_Test(Test_X,weigt,w,beta_hat,L_M[0])
#                RMS[kf_i] = sqrt(mean_squared_error(Test_Y, Y_predict))
#                print("error dar split",RMS[kf_i])
                acc[kf_i]=predict_new(Test_Y,Y_predict)
                print("error dar split",acc[kf_i])
                kf_i=kf_i+1
        pred_chain[k]=np.sum(acc)/5
#        pred_RMSE[k] =np.sum(RMS)/5
        print("miangin",pred_chain[k])
    n_hid=100#L_M[np.argmax(pred_chain)] 
    CC=10#C[np.argmax(pred_chain)] 
    num_layer = num_layer[np.argmin(pred_chain)]
#    y_train_mnist=convert_to_one_hot(y_train_mnist,10).T
#    y_test_mnist=convert_to_one_hot(y_test_mnist,10).T 
#    X_train_mnist=X_train_mnist
#    X_test_mnist=X_test_mnist
    PY_train=convert_to_one_hot(PY_train,10).T
    PY_test=convert_to_one_hot(PY_test,10).T
    for i in range(20):
        print(i)
        weigt,w = DeepELM_wights(PX_train , PY_train , n_hid , num_layer, CC)
        beta_hat = Deep_ELM_Train(PX_train,PY_train , weigt,w,n_hid,CC)
        Y_predict_test=Deep_ELM_Test(PX_test,weigt,w,beta_hat,n_hid)
        Y_predict_train=Deep_ELM_Test(PX_train,weigt,w,beta_hat,n_hid)
        accuracy_test[i]=predict_new(PY_test,Y_predict_test)
        accuracy_train[i]=predict_new(PY_train,Y_predict_train)
#        RMSE_test[i] = sqrt(mean_squared_error(SY_test, Y_predict_test))
#        RMSE_train[i] = sqrt(mean_squared_error(SY_train, Y_predict_train))
    final_acc_test= np.sum(accuracy_test)/20  
    final_acc_train= np.sum(accuracy_train)/20 
#    final_rmse_test= np.sum(RMSE_test)/20  
#    final_rmse_train= np.sum(RMSE_train)/20      
    return final_acc_test,final_acc_train
#%%
acc_test,acc_train = ELM_main()    