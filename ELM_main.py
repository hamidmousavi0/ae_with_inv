# -*- coding: utf-8 -*-
"""
Created on Sat Jul 21 14:43:17 2018

@author: admin
"""
import time
from  dataset import load_mnist
from dataset import load_sat
from dataset import load_duke
from dataset import load_hill_valley,load_olivetti_faces
from dataset import load_usps,load_cars,load_leaves
import numpy as np 
from sklearn.model_selection import KFold 
from predict import predict_new,convert_to_one_hot
from basic_elm import ELM_test,ELM_train
def ELM_main():    
#    mnist = load_mnist()
#    X_train_mnist = mnist.train.images
#    y_train_mnist = mnist.train.labels
#    X_test_mnist =  mnist.test.images
#    y_test_mnist = mnist.test.labels  
#    sat_tr,sat_te = load_sat()
#    sat_train = sat_tr[:,0:36]
#    sat_train = sat_train/np.max(sat_train)
#    sat_train_target = sat_tr[:,36]
#    sat_train_target[np.where(sat_train_target==1)]=0
#    sat_train_target[np.where(sat_train_target==2)]=1
#    sat_train_target[np.where(sat_train_target==3)]=2
#    sat_train_target[np.where(sat_train_target==4)]=3
#    sat_train_target[np.where(sat_train_target==5)]=4
#    sat_train_target[np.where(sat_train_target==6)]=5
#    sat_train_target[np.where(sat_train_target==7)]=6
#    sat_train_target = sat_train_target.astype(int)
#    sat_test = sat_te[:,0:36]
#    sat_test = sat_test/np.max(sat_test)
#    sat_test_target = sat_te[:,36]
#    sat_test_target[np.where(sat_test_target==1)]=0
#    sat_test_target[np.where(sat_test_target==2)]=1
#    sat_test_target[np.where(sat_test_target==3)]=2
#    sat_test_target[np.where(sat_test_target==4)]=3
#    sat_test_target[np.where(sat_test_target==5)]=4
#    sat_test_target[np.where(sat_test_target==6)]=5
#    sat_test_target[np.where(sat_test_target==7)]=6
#    sat_test_target = sat_test_target.astype(int)
#    duke,duke_target=load_duke()
#    permutation = list(np.random.permutation(duke.shape[1]))
#    shuffled_duke_X = duke[:, permutation]
#    shuffled_duke_Y = duke_target[:, permutation].reshape((duke_target.shape[0],duke.shape[1]))
#    duke_train = shuffled_duke_X[:,0:29].T
#    duke_train_traget = shuffled_duke_Y[:,0:29].T
#    duke_train_traget[np.where(duke_train_traget==-1),:]=0
#    duke_train_traget = duke_train_traget.astype(int)
#    duke_test = shuffled_duke_X[:,29:44].T
#    duke_test_traget = shuffled_duke_Y[:,29:44].T
#    duke_test_traget[np.where(duke_train_traget==-1),:]=0
#    duke_test_traget = duke_test_traget.astype(int)
#    hill_train,hill_test=load_hill_valley()
#    hill_valey_train = hill_train[:,0:100] 
#    hill_valey_train = hill_valey_train/np.max(hill_valey_train)
#    hill_valey_train_target = hill_train[:,100]
#    hill_valey_train_target = hill_valey_train_target.astype(int)
#    hill_valey_test = hill_test[:,0:100] 
#    hill_valey_test= hill_valey_test/np.max(hill_valey_test)
#    hill_valey_test_target = hill_test[:,100]
#    hill_valey_test_target = hill_valey_test_target.astype(int)
#    usps_train,usps_train_target,usps_test,usps_test_target = load_usps()
#    usps_train_target[np.where(usps_train_target==1)]=0
#    usps_train_target[np.where(usps_train_target==2)]=1
#    usps_train_target[np.where(usps_train_target==3)]=2
#    usps_train_target[np.where(usps_train_target==4)]=3
#    usps_train_target[np.where(usps_train_target==5)]=4
#    usps_train_target[np.where(usps_train_target==6)]=5
#    usps_train_target[np.where(usps_train_target==7)]=6
#    usps_train_target[np.where(usps_train_target==8)]=7
#    usps_train_target[np.where(usps_train_target==9)]=8
#    usps_train_target[np.where(usps_train_target==10)]=9
#    usps_train_target = usps_train_target.astype(int)
#    usps_test_target[np.where(usps_test_target==1)]=0
#    usps_test_target[np.where(usps_test_target==2)]=1
#    usps_test_target[np.where(usps_test_target==3)]=2
#    usps_test_target[np.where(usps_test_target==4)]=3
#    usps_test_target[np.where(usps_test_target==5)]=4
#    usps_test_target[np.where(usps_test_target==6)]=5
#    usps_test_target[np.where(usps_test_target==7)]=6
#    usps_test_target[np.where(usps_test_target==8)]=7
#    usps_test_target[np.where(usps_test_target==9)]=8
#    usps_test_target[np.where(usps_test_target==10)]=9
#    usps_test_target = usps_test_target.astype(int)
    face=load_olivetti_faces()
    face_data = face.data
    face_target = face.target
    permutation = list(np.random.permutation(face_data.shape[0]))
    shuffled_face_X = face_data[permutation,:]
    shuffled_face_Y = face_target[permutation]
    face_train = shuffled_face_X[0:300,:]
    face_train_traget = shuffled_face_Y[0:300]
    face_train_traget = face_train_traget.astype(int)
    face_test = shuffled_face_X[300:400,:]
    face_test_traget = shuffled_face_Y[300:400]
    face_test_traget = face_test_traget.astype(int)
#    L_M=[15000]
#    C=[10**6]
#    #L_M=[1000]
#    L_O=[10*i for i in range(2,21)]
#    acc=np.zeros((5))
    accuracy=np.zeros((10))
#    pred_chain=np.zeros(len(C))
#    for k in range(len(L_M)):
#        print(k)
#        for kk in range(len(C)):
#            print(kk)
#            kf = KFold(n_splits=5)
#            kf_i=0
#            for train_index, test_index in kf.split(X_train_mnist):
#                Train_X, Test_X = X_train_mnist[train_index], X_train_mnist[test_index]
#                Train_Y, Test_Y = y_train_mnist[train_index], y_train_mnist[test_index]
#                Train_Y=convert_to_one_hot(Train_Y,10).T
#                Test_Y=convert_to_one_hot(Test_Y,10).T
#                W,Beta_hat,b,Y=ELM_train(Train_X,Train_Y,L_M[k],C[kk])
#                Y_predict=ELM_test(Test_X,W,b,Beta_hat)
#                acc[kf_i]=predict_new(Test_Y,Y_predict)
#                kf_i=kf_i+1
#            pred_chain[kk]=np.sum(acc)/5  
    n_hid=40#L_M[np.argmax(pred_chain)] 
    CC=10**6#C[np.argmax(pred_chain)] 
#    y_train_mnist=convert_to_one_hot(y_train_mnist,10).T
#    y_test_mnist=convert_to_one_hot(y_test_mnist,10).T 
#    X_train_mnist=X_train_mnist
#    X_test_mnist=X_test_mnist
#    duke_train_traget=convert_to_one_hot(duke_train_traget,2).T
#    duke_test_traget=convert_to_one_hot(duke_test_traget,2).T 
#    duke_train=duke_train
#    duke_test=duke_test
#    sat_train_target=convert_to_one_hot(sat_train_target,7).T
#    sat_test_target=convert_to_one_hot(sat_test_target,7).T 
#    sat_train=sat_train
#    sat_test=sat_test
#    hill_valey_train_target=convert_to_one_hot(hill_valey_train_target,2).T
#    hill_valey_test_target=convert_to_one_hot(hill_valey_test_target,2).T 
#    hill_valey_train=hill_valey_train
#    hill_valey_test=hill_valey_test
#    usps_train_target=convert_to_one_hot(usps_train_target,10).T
#    usps_test_target=convert_to_one_hot(usps_test_target,10).T 
#    usps_train=usps_train.T
#    usps_test=usps_test.T
    face_train_traget=convert_to_one_hot(face_train_traget,40).T
    face_test_traget=convert_to_one_hot(face_test_traget,40).T 
    face_train=face_train
    face_test=face_test

    start = time.time()  
    for i in range(10):
        print(i)
        W,Beta_hat,b,Y=ELM_train(face_train,face_train_traget,n_hid,CC)
        Y_predict=ELM_test(face_test,W,b,Beta_hat)
        accuracy[i]=predict_new(face_test_traget,Y_predict)
    final_acc= np.sum(accuracy)/10   
    stop = time.time()
    return final_acc,stop-start
#%%
 

acc,tim=ELM_main()

