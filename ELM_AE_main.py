# -*- coding: utf-8 -*-
"""
Created on Sat Jul 21 14:35:46 2018

@author: admin
"""
import numpy as np
from  dataset import load_mnist
from basic_ELM_AE import ELM_AE
import matplotlib.pyplot as plt

def main_ELM_AE():
    mnist=load_mnist()
    X_train_mnist = mnist.train.images
    y_train_mnist = mnist.train.labels
    X_test_mnist =  mnist.test.images
    y_test_mnist = mnist.test.labels   
    X_train_zero = X_train_mnist[np.where(y_train_mnist == 0)][:]
    X_train_one = X_train_mnist[np.where(y_train_mnist == 1)][:]
    X_train_two = X_train_mnist[np.where(y_train_mnist == 2)][:]
    X_train_three = X_train_mnist[np.where(y_train_mnist == 3)][:] 
    X_train_four = X_train_mnist[np.where(y_train_mnist == 4)][:] 
    X_train_five = X_train_mnist[np.where(y_train_mnist == 5)][:] 
    X_train_six = X_train_mnist[np.where(y_train_mnist == 6)][:] 
    X_train_seven = X_train_mnist[np.where(y_train_mnist == 7)][:] 
    X_train_eight = X_train_mnist[np.where(y_train_mnist == 8)][:] 
    X_train_nine = X_train_mnist[np.where(y_train_mnist == 9)][:] 
            
    numhid=20
    C=10**6
    Train_X=X_train_nine
    beta=ELM_AE(Train_X,numhid,C)
    beta_hat=beta.reshape((beta.shape[0],28,28))    
    plt.figure(figsize=(5,5))
    for i in range(beta.shape[0]):
        plt.subplot(5,4,i+1)
        plt.xticks([])
        plt.yticks([])
        plt.grid('off')
        plt.imshow(beta_hat[i], cmap=plt.cm.binary)
    return beta_hat
beta=main_ELM_AE()