# -*- coding: utf-8 -*-
"""
Created on Wed Aug  8 10:29:46 2018

@author: Hamid
"""
from Predict import predict_new
from ConvertToonehot import convert_to_one_hot
import dataset
from DeepModel_new import five_layer_model
def Deep_Main():
    X_train_mnist = dataset.mnist.train.images
    train_x = X_train_mnist.T
    y_train_mnist = dataset.mnist.train.labels
    X_test_mnist =  dataset.mnist.test.images
    test_x = X_test_mnist.T
    y_test_mnist = dataset.mnist.test.labels   
    train_y = convert_to_one_hot(y_train_mnist, 10)
    test_y = convert_to_one_hot(y_test_mnist, 10)
    n_x = train_x.shape[0]
    n_h1=100
    n_h2=64
    n_h3=64
    n_h4=64
    n_y=10
    layers_dims = (n_x, n_h1,n_h2,n_h3,n_h4, n_y)
    parameters = five_layer_model(train_x, train_y, layers_dims,learning_rate =  0.001, num_iterations = 10, print_cost = True,keep_prob=0.8,beta1=0.99,beta2 = 0.999,epsilon = 1e-8,mini_batch_size = 64)
    pred_train = predict_new(train_x, train_y, parameters)
    pred_test = predict_new(test_x, test_y, parameters)
    return pred_train,pred_test
#%%
p_train,p_test = Deep_Main()    
    