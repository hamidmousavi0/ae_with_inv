# -*- coding: utf-8 -*-
"""
Created on Sun Aug 19 15:59:28 2018

@author: Hamid
"""
from basic_new_ELMAE import new_ELMAE
from ConvertToonehot import convert_to_one_hot
import numpy as np 
from dataset import load_mnist
import matplotlib.pyplot as plt
from DeepModel_new import five_layer_model
from RandomMinibatch import random_mini_batches
from Cost import compute_cost_softmax
from Sigmoid import sigmoid
from Relu import relu,relu_backward
from Softmax import softmax
#%%
def new_ELMAE_main():
    mnist = load_mnist()
    X_train_mnist = mnist.train.images
    y_train_mnist = mnist.train.labels
    X_test_mnist =  mnist.test.images
    y_test_mnist = mnist.test.labels  
#    sat_tr,sat_te = load_sat()
#    sat_train = sat_tr[:,0:36]
#    sat_train = sat_train/np.max(sat_train)
#    sat_train_target = sat_tr[:,36]
#    sat_train_target[np.where(sat_train_target==1)]=0
#    sat_train_target[np.where(sat_train_target==2)]=1
#    sat_train_target[np.where(sat_train_target==3)]=2
#    sat_train_target[np.where(sat_train_target==4)]=3
#    sat_train_target[np.where(sat_train_target==5)]=4
#    sat_train_target[np.where(sat_train_target==6)]=5
#    sat_train_target[np.where(sat_train_target==7)]=6
#    sat_train_target = sat_train_target.astype(int)
#    sat_test = sat_te[:,0:36]
#    sat_test = sat_test/np.max(sat_test)
#    sat_test_target = sat_te[:,36]
#    sat_test_target[np.where(sat_test_target==1)]=0
#    sat_test_target[np.where(sat_test_target==2)]=1
#    sat_test_target[np.where(sat_test_target==3)]=2
#    sat_test_target[np.where(sat_test_target==4)]=3
#    sat_test_target[np.where(sat_test_target==5)]=4
#    sat_test_target[np.where(sat_test_target==6)]=5
#    sat_test_target[np.where(sat_test_target==7)]=6
#    sat_test_target = sat_test_target.astype(int)
#    duke,duke_target=load_duke()
#    permutation = list(np.random.permutation(duke.shape[1]))
#    shuffled_duke_X = duke[:, permutation]
#    shuffled_duke_Y = duke_target[:, permutation].reshape((duke_target.shape[0],duke.shape[1]))
#    duke_train = shuffled_duke_X[:,0:29].T
#    duke_train_traget = shuffled_duke_Y[:,0:29].T
#    duke_train_traget[np.where(duke_train_traget==-1),:]=0
#    duke_train_traget = duke_train_traget.astype(int)
#    duke_test = shuffled_duke_X[:,29:44].T
#    duke_test_traget = shuffled_duke_Y[:,29:44].T
#    duke_test_traget[np.where(duke_train_traget==-1),:]=0
#    duke_test_traget = duke_test_traget.astype(int)
#    hill_train,hill_test=load_hill_valley()
#    hill_valey_train = hill_train[:,0:100] 
#    hill_valey_train = hill_valey_train/np.max(hill_valey_train)
#    hill_valey_train_target = hill_train[:,100]
#    hill_valey_train_target = hill_valey_train_target.astype(int)
#    hill_valey_test = hill_test[:,0:100] 
#    hill_valey_test= hill_valey_test/np.max(hill_valey_test)
#    hill_valey_test_target = hill_test[:,100]
#    hill_valey_test_target = hill_valey_test_target.astype(int)
#    usps_train,usps_train_target,usps_test,usps_test_target = load_usps()
#    usps_train_target[np.where(usps_train_target==1)]=0
#    usps_train_target[np.where(usps_train_target==2)]=1
#    usps_train_target[np.where(usps_train_target==3)]=2
#    usps_train_target[np.where(usps_train_target==4)]=3
#    usps_train_target[np.where(usps_train_target==5)]=4
#    usps_train_target[np.where(usps_train_target==6)]=5
#    usps_train_target[np.where(usps_train_target==7)]=6
#    usps_train_target[np.where(usps_train_target==8)]=7
#    usps_train_target[np.where(usps_train_target==9)]=8
#    usps_train_target[np.where(usps_train_target==10)]=9
#    usps_train_target = usps_train_target.astype(int)
#    usps_test_target[np.where(usps_test_target==1)]=0
#    usps_test_target[np.where(usps_test_target==2)]=1
#    usps_test_target[np.where(usps_test_target==3)]=2
#    usps_test_target[np.where(usps_test_target==4)]=3
#    usps_test_target[np.where(usps_test_target==5)]=4
#    usps_test_target[np.where(usps_test_target==6)]=5
#    usps_test_target[np.where(usps_test_target==7)]=6
#    usps_test_target[np.where(usps_test_target==8)]=7
#    usps_test_target[np.where(usps_test_target==9)]=8
#    usps_test_target[np.where(usps_test_target==10)]=9
#    usps_test_target = usps_test_target.astype(int)
#    face=load_olivetti_faces()
#    face_data = face.data
#    face_target = face.target
#    permutation = list(np.random.permutation(face_data.shape[0]))
#    shuffled_face_X = face_data[permutation,:]
#    shuffled_face_Y = face_target[permutation]
#    face_train = shuffled_face_X[0:350,:]
#    face_train_traget = shuffled_face_Y[0:350]
#    face_train_traget = face_train_traget.astype(int)
#    face_test = shuffled_face_X[350:400,:]
#    face_test_traget = shuffled_face_Y[350:400]
#    face_test_traget = face_test_traget.astype(int)
    C=[2**10]
    reconst_X,H3_train,H3_test=new_ELMAE(X_train_mnist,X_test_mnist,C,500,2)
    
    
    plt.figure(figsize=(5,5))
    for i in range(10):
        plt.subplot(5,4,i+1)
        plt.xticks([])
        plt.yticks([])
        plt.grid('off')
        plt.imshow(reconst_X[i].reshape((28,28)), cmap=plt.cm.binary)
    y_train_mnist=convert_to_one_hot(y_train_mnist,10)
    y_test_mnist=convert_to_one_hot(y_test_mnist,10)
#    duke_train_traget=convert_to_one_hot(duke_train_traget,2)
#    duke_test_traget=convert_to_one_hot(duke_test_traget,2)
#    sat_train_target=convert_to_one_hot(sat_train_target,7)
#    sat_test_target=convert_to_one_hot(sat_test_target,7)
#    hill_valey_train_target=convert_to_one_hot(hill_valey_train_target,2)
#    hill_valey_test_target=convert_to_one_hot(hill_valey_test_target,2) 
#    usps_train_target=convert_to_one_hot(usps_train_target,10)
#    usps_test_target=convert_to_one_hot(usps_test_target,10)
#    face_train_traget=convert_to_one_hot(face_train_traget,40)
#    face_test_traget=convert_to_one_hot(face_test_traget,40) 
    parameters = model_deep(H3_train, y_train_mnist,learning_rate =  0.001, num_iterations = 10, print_cost = True,beta1=0.99,beta2 = 0.999,epsilon = 1e-8,mini_batch_size = 128)
    pred_train = predict_new(H3_train, y_train_mnist, parameters)
    pred_test = predict_new(H3_test, y_test_mnist, parameters)
    return reconst_X  ,pred_train  ,pred_test
#%%

def model_deep(X,Y,learning_rate =  0.001, num_iterations = 100, print_cost = True,beta1=0.99,beta2 = 0.999,epsilon = 1e-8,mini_batch_size = 64):
    grads = {}
    costs = []  
    costs_temp =[]
    t=0
    seed=10
    m = X.shape[1]  
    n_x = X.shape[0]
    n_h1=64
    n_h2=64
    n_h3=64
    n_h4=64
    n_y=10
    layers_dims=(n_x, n_h1,n_h2,n_h3,n_h4,n_y)
    parameters= initialize_parameters_he(layers_dims)
    v,s=initialize_adam(parameters)
    for i in range(0, num_iterations):
        seed = seed + 1
        minibatches = random_mini_batches(X, Y, mini_batch_size, seed)
        for minibatch in minibatches:
            (minibatch_X, minibatch_Y) = minibatch
            A4,cache=forward_propagation(minibatch_X,parameters)
            cost = compute_cost_softmax(A4,minibatch_Y)
            grads= backward_propagation(minibatch_X, minibatch_Y, cache)
            t=t+1
            parameters,v,s= update_parameters_with_adam(parameters, grads, v, s, t, learning_rate ,
                                  beta1 , beta2 ,  epsilon )
            costs_temp.append(cost)
        costs_temp_new = np.array(costs_temp)
        costs_temp_new = np.mean(costs_temp_new)
        if print_cost :
           print("Cost after iteration {}: {}".format(i, np.squeeze(costs_temp_new)))
        if print_cost :
           costs.append(costs_temp_new)
    plt.figure()       
    plt.plot(np.squeeze(costs))
    plt.ylabel('cost')
    plt.xlabel('iterations (per tens)')
    plt.title("Learning rate =" + str(learning_rate))
    plt.show()
    
    return parameters
#%%
def initialize_parameters_he(layers_dims):
    parameters = {}
    parameters['W1'] = np.random.randn(layers_dims[1],layers_dims[0])*np.sqrt(2/layers_dims[0])
    parameters['b1'] = np.zeros((layers_dims[1],1))
    parameters['W2'] = np.random.randn(layers_dims[2],layers_dims[1])*np.sqrt(2/layers_dims[1])
    parameters['b2'] = np.zeros((layers_dims[2],1))
    parameters['W3'] = np.random.randn(layers_dims[4],layers_dims[3])*np.sqrt(2/layers_dims[3])
    parameters['b3'] = np.zeros((layers_dims[4],1))
    parameters['W4'] = np.random.randn(layers_dims[5],layers_dims[4])*np.sqrt(2/layers_dims[4])
    parameters['b4'] = np.zeros((layers_dims[5],1))
        
    return parameters    
#%%  
def initialize_adam(parameters) :
    L = len(parameters) // 2 
    v = {}
    s = {}
    for l in range(L):
        v["dW" + str(l+1)] = np.zeros_like(parameters["W" + str(l + 1)])
        v["db" + str(l+1)] = np.zeros_like(parameters["b" + str(l + 1)])
        s["dW" + str(l+1)] = np.zeros_like(parameters["W" + str(l + 1)])
        s["db" + str(l+1)] = np.zeros_like(parameters["b" + str(l + 1)])
    
    return v, s
#%%

def update_parameters_with_adam(parameters, grads, v, s, t, learning_rate = 0.01,
                                beta1 = 0.9, beta2 = 0.999,  epsilon = 1e-8): 
    L = len(parameters) // 2               
    v_corrected = {}                         
    s_corrected = {}                       
    for l in range(L):
        v["dW" + str(l+1)] = beta1 * v["dW" + str(l + 1)] + (1 - beta1) * grads['dW' + str(l + 1)]
        v["db" + str(l+1)] = beta1 * v["db" + str(l + 1)] + (1 - beta1) * grads['db' + str(l + 1)]
        v_corrected["dW" + str(l+1)] = v["dW" + str(l + 1)] / (1 - np.power(beta1, t))
        v_corrected["db" + str(l+1)] = v["db" + str(l + 1)] / (1 - np.power(beta1, t))
        s["dW" + str(l+1)] = beta2 * s["dW" + str(l + 1)] + (1 - beta2) * np.power(grads['dW' + str(l + 1)], 2)
        s["db" + str(l+1)] = beta2 * s["db" + str(l + 1)] + (1 - beta2) * np.power(grads['db' + str(l + 1)], 2)
        s_corrected["dW" + str(l+1)] = s["dW" + str(l + 1)] / (1 - np.power(beta2, t))
        s_corrected["db" + str(l+1)] = s["db" + str(l + 1)] / (1 - np.power(beta2, t))
        parameters["W" + str(l+1)] = parameters["W" + str(l + 1)] - learning_rate * v_corrected["dW" + str(l + 1)] / np.sqrt(s["dW" + str(l + 1)] + epsilon)
        parameters["b" + str(l+1)] = parameters["b" + str(l + 1)] - learning_rate * v_corrected["db" + str(l + 1)] / np.sqrt(s["db" + str(l + 1)] + epsilon)
    return parameters, v, s
#%%
def forward_propagation(X, parameters):
    W1 = parameters["W1"]
    b1 = parameters["b1"]
    W2 = parameters["W2"]
    b2 = parameters["b2"]  
    W3 = parameters["W3"]
    b3 = parameters["b3"]
    W4 = parameters["W4"]
    b4 = parameters["b4"]  
    Z1 = np.dot(W1, X) + b1
    A1,cash1 = relu(Z1)                                                                    
    Z2 = np.dot(W2, A1) + b2
    A2,cash2 = relu(Z2)
    Z3 = np.dot(W3, A2) + b3
    A3,cash3 = relu(Z3)                                                                   
    Z4 = np.dot(W4, A3) + b4
    A4,cash4 = softmax(Z4)     
    cache = (Z1,A1, W1, b1, Z2,A2,W2,b2,Z3,A3, W3, b3, Z4,A4,W4,b4)
    return  A4,cache
#%%
def backward_propagation(X, Y, cache1):  
    m = X.shape[1]
    (Z1,A1, W1, b1, Z2, A2, W2, b2,Z3,A3, W3, b3, Z4, A4, W4, b4) = cache1
    dZ4 =A4 -Y
    dW4 = 1./m * np.dot(dZ4, A3.T)
    db4 = 1./m * np.sum(dZ4, axis=1, keepdims = True)
    dA3 = np.dot(W4.T, dZ4)            
    dZ3 = relu_backward(dA3,Z3)
    dW3 = 1./m * np.dot(dZ3, A2.T)
    db3 = 1./m * np.sum(dZ3, axis=1, keepdims = True)
    dA2 = np.dot(W3.T, dZ3)[0:64,:]
    dZ2 = relu_backward(dA2,Z2)
    dW2 = 1./m * np.dot(dZ2, A1.T)
    db2 = 1./m * np.sum(dZ2, axis=1, keepdims = True)
    dA1 = np.dot(W2.T, dZ2)
    dZ1 = relu_backward(dA1,Z1)
    dW1 = 1./m * np.dot(dZ1, X.T)
    db1 = 1./m * np.sum(dZ1, axis=1, keepdims = True)
    
    gradients = {"dZ4": dZ4, "dW4": dW4, "db4": db4,
                 "dA3":dA3,"dZ3": dZ3, "dW3": dW3, "db3": db3,
                 "dA2":dA2,"dZ2": dZ2, "dW2": dW2, "db2": db2,
                 "dA1":dA1,"dZ1": dZ1, "dW1": dW1, "db1": db1}
    
    return gradients
#%%    
def predict_new(X, y, parameters):
    m = X.shape[1]
    p = np.zeros((1,m))
    y_index=np.zeros((1,m))
    y_index=(np.argmax(y,axis=0)).reshape((1,m))
    probas, caches = forward_propagation(X, parameters)
    for i in range(0, probas.shape[1]):
            max_index=np.where(probas[:,i]==probas[:,i].max())
            p[0,i] = max_index[0][0]
    print("Accuracy: "  + str(np.sum((p == y_index)/m)))
        
    return np.sum((p == y_index)/m)   
#%%
reconst_X  ,pred_train  ,pred_test=new_ELMAE_main()
print(pred_train)
print(pred_test)
    